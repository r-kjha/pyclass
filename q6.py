#wap to create a class Square and define 2 methods for area and perimeter

class Square:
    def __init__(self,len):
        self.len=len

    def area(self):
        return self.len**2

    def peri(self):
        return 4*self.len

obj=Square(5)

print("Area: ",obj.area())
print("Perimeter: ",obj.peri())