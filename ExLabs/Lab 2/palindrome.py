#  Write a program to find variable is palindrome or not(Do not use built in function and
# slice) using while or for loop
# a) 1818
# b) aba

data = input ("Please enter the word/num to check palindrome: ")
revData = ""
for x in data:
    revData = x + revData

print("The Reverse word of ", data , " is ", revData)
if(data == revData):
    print("It is Palindrome")
else:
    print("It is not Palindrome")