# Write a program to find the fibonacci series using for and while loop
# a) 0,1,1,2,3,5,8,......

a = 0
b = 1

terms = int(input("Enter the number of terms for the series: "))
count = 0
print(a)
print(b)
while(count<terms):
    print(a+b)
    b = a+b
    a = b-a
    count+=1