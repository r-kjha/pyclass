#  Write a Program whether the year is leap year or not
# Conditions:
# The entered year must be divisible by 4
# The entered year must be divisible by 400 but not by 100

year = int(input("Please enter any year: "))

if (year % 4 == 0 or (year % 400 == 0 and year % 100 != 0)):
        print(year, " is a leap year")
else:
    print(year," is not a leap year")
