# Write a program to display the last digit of a number and ensure that num is divisible by 3

num = int(input("Please enter any number: "))

last = num % 10

print("Last Digit of ",num," is ", last)

print (last," is divisible by 3? ",((last % 3) == 0))