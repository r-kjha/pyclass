# Write a program to calculate electric bill(accept no of unit from user) according to the
# following criteria
# First 100 unit=No charge
# Next 100 unit = Rs. 5 per unit
# After 200 Unit = Rs. 10 per unit

billUnits = int(input("Please Enter the Total Units: "))

if (billUnits>200):    
    billAmount = 500 + (billUnits-200)*10
elif(billUnits>100):
    billAmount = (billUnits - 100)*5
else:
    billAmount = 0

print("Your Total Bill Amount is: Rs.", billAmount)