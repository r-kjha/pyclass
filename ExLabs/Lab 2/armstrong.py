#  Write a program to find Armstrong
# a) 153=1^3+5^3+3^3
# b) 1634 = 1^4+6^4+3^4+4^4

num = int(input("Please enter the number: "))

temp = num # saving original number

# Counting Digits
# digits = 0
# while (temp != 0):
#     temp //= 10
#     digits += 1

digits = len(str(num))

# Finding Armstrong

newNum = 0
while (temp != 0):
    last = temp % 10
    newNum = newNum + last ** digits 
    temp //= 10

print("New Number formed: ", newNum)

if newNum == num:
    print("It is Armstrong")
else:
    print("It is not Armstrong")