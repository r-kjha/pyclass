# Bitwise OR demonstration
num1 = 13  # binary 1101
num2 = 7   # binary 0111

result = num1 | num2  # binary 1111

print(f"Bitwise OR of {num1} and {num2}: {result}")
