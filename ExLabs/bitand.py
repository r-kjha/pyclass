# Bitwise AND demonstration
num1 = 13 # binary 1101
num2 = 7 # binary 0111

result = num1 & num2 # binary 0101

print(f"Bitwise AND of {num1} and {num2}: {result}")
